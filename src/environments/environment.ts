// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig:{
    
    apiKey: "AIzaSyBb-yPjnCz3KGJYfULDtty8Y8rTxvQg56Y",
    authDomain: "blog-ysf.firebaseapp.com",
    databaseURL: "https://blog-ysf.firebaseio.com",
    projectId: "blog-ysf",
    storageBucket: "blog-ysf.appspot.com",
    messagingSenderId: "65504768578",
    appId: "1:65504768578:web:8b5bf4201b08579c6173db",
    measurementId: "G-CE66YTCQP8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
