import { Component, OnInit } from '@angular/core';
import { LoginService } from '../shared/login.service';
import * as firebase from 'firebase'
import { Router } from '@angular/router';
declare let $:any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private loginService: LoginService,private router: Router) { }
  test =false;

  ngOnInit() {   
    firebase.auth().onAuthStateChanged(
      (userSesion)=>{
        if(userSesion){
          console.log('vous etes connecté');
          this.test = true;
        }else {
          console.log("vous n'etes pas connecté");
          this.test = false;
        }
      }
    )
  }

  onLogout(){
    this.loginService.logout().then(
      ()=>{
        this.router.navigate(['/home']);
      }
      )
  }
}
