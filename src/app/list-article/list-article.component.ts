import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../shared/article.service';
import { map } from 'rxjs/operators';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
//import { Articles } from '../articles';


declare let $:any;

@Component({
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.css']
})
export class ListArticleComponent implements OnInit {
  form:FormGroup;
  constructor(private formBuilder: FormBuilder,private articleService:ArticleService) { }
  
  article;
  
  ngOnInit() {

    this.form = this.formBuilder.group({
      Titre:['', Validators.required],
      Url: ['', Validators.required],
      Texte:['', Validators.required],
      Auteur:['', Validators.required]
    });

    this.display();

    //$(document).ready(()=>{
   //   $('.modal').modal();
  //  })
  }

display(){
    this.article = this.articleService.getArt().pipe(
      map(actions => actions.map(a =>{
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return{id,...data};
      }))
    );
  }
addArt(){
  if(this.form.valid){
    let article = {
      Titre: this.form.value.Titre,
      Url: this.form.value.Url,
      Texte: this.form.value.Texte,
      Auteur: this.form.value.Auteur,  
    };
    this.articleService.createArt(article);
    this.resetForm();
  }
}

removeArt(id){
  if(confirm('Voulez-vous vraiment supprimer cette article')){
    this.articleService.deleteArt(id);
  }
}

resetForm(){
  this.form.reset();
}
}
