import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../shared/article.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private articleService:ArticleService) { }
  
  article;

  ngOnInit() {
    this.display();
  }
  display(){
    this.article = this.articleService.getArt().pipe(
      map(actions => actions.map(a =>{
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return{id,...data};
      }))
    );
  }
}
