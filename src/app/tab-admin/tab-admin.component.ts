import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/shared/login.service';

@Component({
  selector: 'app-tab-admin',
  templateUrl: './tab-admin.component.html',
  styleUrls: ['./tab-admin.component.css']
})
export class TabAdminComponent implements OnInit {
  constructor(
    private loginservice: LoginService,
    private formBuilder:FormBuilder,
    private router : Router
    ) { }
  registerForm:FormGroup;
  ngOnInit() {
    this.initRegisterForm();
  }
  initRegisterForm(){
    this.registerForm = this.formBuilder.group({
      email: ['',[Validators.required,Validators.email]],
      password: ['',[Validators.required,Validators.minLength(6)]]
    });
  }
  onConnect(){
    let email = this.registerForm.get("email").value;
    let password = this.registerForm.get("password").value;
    this.loginservice.login(email,password).then(
      ()=>{
        console.log('success');
        this.router.navigate(["/admin"])
      }).catch(
        (error)=>{
        console.error(error)
        alert("Erreur de d'identifiant ou de mot de passe")
        }
      )
      
  }
}
