import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore'

import { AppComponent } from './app.component';
//import { environment } from 'src/environments/environment';
import { environment } from 'src/environments/environment.prod';

import { ListArticleComponent } from './list-article/list-article.component';
import { ArticleService } from './shared/article.service';
import { TabAdminComponent } from './tab-admin/tab-admin.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PresentationComponent } from './presentation/presentation.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import * as firebase from 'firebase';
firebase.initializeApp(environment.firebaseConfig);


@NgModule({
  declarations: [
    AppComponent,
    ListArticleComponent,
    TabAdminComponent,
    NavbarComponent,
    PresentationComponent,
    ContactComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    FormsModule,
    ReactiveFormsModule
   
  ],

  providers: [ArticleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
