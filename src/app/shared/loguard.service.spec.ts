import { TestBed } from '@angular/core/testing';

import { LoguardService } from './loguard.service';

describe('LoguardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoguardService = TestBed.get(LoguardService);
    expect(service).toBeTruthy();
  });
});
