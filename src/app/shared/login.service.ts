import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }

//inscription
register(email:string, password:string){
  return new Promise((resolve, reject)=>{
    firebase.auth().createUserWithEmailAndPassword(email, password).then(()=>{
      resolve();
      console.log("creation avec succes")
    }).catch(
      (error)=>{
        reject(error);
      }
    )
  }
  )}
//canexion
login(email:string, password:string){
  return new Promise((resolve, reject)=>{
    firebase.auth().signInWithEmailAndPassword(email, password).then(()=>{
      resolve();
      console.log("conexion reussie")
    }).catch(
      (error)=>{
        reject(error);
      }
    )
  }
  )}

  //deconexion
  logout(){
    return new Promise((resolve, reject)=>{
      if(firebase.auth().currentUser){
        firebase.auth().signOut();
        resolve();
      }else{
        reject();
      }
    })
  }
}
