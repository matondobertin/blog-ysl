import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private firestore: AngularFirestore) { }

  getArt(){
    return this.firestore.collection('Articles').snapshotChanges();
  }

  createArt(x){
    return this.firestore.collection('Articles').add(x);
  }

  deleteArt(id){
    return this.firestore.collection('Articles').doc(id).delete();
  }
}


