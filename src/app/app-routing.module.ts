import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PresentationComponent } from './presentation/presentation.component';
import { ContactComponent } from './contact/contact.component';
import { TabAdminComponent } from './tab-admin/tab-admin.component';
import { ListArticleComponent } from './list-article/list-article.component';
import { LoguardService } from './shared/loguard.service';



const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'presentation', component: PresentationComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'connexion', component: TabAdminComponent},
  { path: 'admin',canActivate:[LoguardService] ,component: ListArticleComponent},
  { path: '', redirectTo:'home', pathMatch: 'full'},
  { path: '**', redirectTo:'home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
